#!/bin/bash

echo
echo "************************************************************************"
echo "**                                                                    **"
echo "**                           FEDORA SETTING                           **"  
echo "**                                                                    **"
echo "************************************************************************"
echo
echo "Este es el script de Rober para configurar tu Fedora 36 Workstation."
sleep 2s
echo
echo "¡Vamos a ello!"
sleep 3s
echo

echo "Empecemos configurando los repositorios de RPM Fusion y de Flathub..."
echo
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
  https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm || { echo "Error instalando los repositorios RPM Fusion"; exit 1; }

sudo dnf update --refresh -y || { echo "Error actualizando el sistema"; exit 1; }
sudo dnf groupupdate core -y || { echo "Error actualizando el grupo core"; exit 1; }
sudo dnf install -y rpmfusion-free-appstream-data rpmfusion-nonfree-appstream-data || { echo "Error instalando datos de AppStream"; exit 1; }

sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo || { echo "Error agregando repositorio de Flathub"; exit 1; }
echo
echo "Continuamos instalando algunos paquetes básicos..."
echo
sudo dnf install -y gstreamer1-libav gstreamer1-plugins-bad-free-extras gstreamer1-plugins-bad-freeworld \
  gstreamer1-plugins-good-extras gstreamer1-plugins-ugly unrar p7zip p7zip-plugins gstreamer1-plugin-openh264 \
  mozilla-openh264 openh264 webp-pixbuf-loader gstreamer1-plugins-bad-free-fluidsynth gstreamer1-plugins-bad-free-wildmidi \
  gstreamer1-svt-av1 libopenraw-pixbuf-loader dav1d file-roller || { echo "Error instalando paquetes básicos"; exit 1; }

echo
while true; do
    read -p "¿Quieres instalar los drivers privativos de NVIDIA (s/n)? " sn
    case $sn in
        [Ss]* )  
            sudo dnf install -y akmod-nvidia xorg-x11-drv-nvidia-cuda || { echo "Error instalando drivers de NVIDIA"; exit 1; }
            sudo dnf update -y || { echo "Error actualizando después de instalar los drivers de NVIDIA"; exit 1; }
            break;;
        [Nn]* ) 
            echo "Saltando la instalación de drivers de NVIDIA..."
            break;;
        * ) echo "Por favor, pulsa 's' o 'n'.";;
    esac
done

echo
while true; do
    read -p "¿Quieres instalar los drivers de Intel para activar la aceleración por hardware (s/n)? " sn
    case $sn in
        [Ss]* )  
            sudo dnf install -y libva-intel-driver intel-media-driver libva libva-utils || { echo "Error instalando drivers de Intel"; exit 1; }
            break;;
        [Nn]* ) 
            echo "Saltando la instalación de drivers de Intel..."
            break;;
        * ) echo "Por favor, pulsa 's' o 'n'.";;
    esac
done

echo
while true; do
    read -p "¿Quieres instalar algunos paquetes para renderizar vídeo (s/n)? " sn
    case $sn in
        [Ss]* )  
            sudo dnf install -y x264 h264enc x265 svt-av1 rav1e || { echo "Error instalando paquetes para renderizar vídeo"; exit 1; }
            break;;
        [Nn]* ) 
            echo "Saltando la instalación de paquetes de renderizado de vídeo..."
            break;;
        * ) echo "Por favor, pulsa 's' o 'n'.";;
    esac
done

echo
echo "Por último, instalaremos herramientas para administrar la configuración de SELinux y el firewall de forma gráfica..."
echo
sudo dnf install -y policycoreutils-gui firewall-config || { echo "Error instalando herramientas gráficas de SELinux y firewall"; exit 1; }
echo
echo "Todo listo, te recomiendo reiniciar el sistema para aplicar correctamente todos los cambios."
echo

while true; do
    read -p "¿Quieres reiniciar el sistema ahora (s/n)? " sn
    case $sn in
        [Ss]* )  
            sudo reboot
            break;;
        [Nn]* ) 
            echo "No se reiniciará el sistema."
            exit;;
        * ) echo "Por favor, pulsa 's' o 'n'.";;
    esac
done