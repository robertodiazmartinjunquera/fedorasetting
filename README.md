# FEDORASETTING

<p>Esto es un script para configurar Fedora cuando lo instalamos desde 0.

Este script esta pensado unicamente para Fedora 36 Workstation y funciona con cualquier entorno de escritorio ya sea GNOME, KDE PLASMA...</p>


# FUNCIONAMIENTO/INSTALACIÓN:

El script debe ejecutarse con permisos de Ejecución para poder funcionar correctamente, después simplemente ejecute el script.

Las ordenes necesarias para ejecutar el script son las siguientes:

```bash
su                          #para entrar como usuario root
```

```bash
chmod +x fedorasetting.sh    #para dar permisos de ejecución al script
```

```bash
./fedorasetting.sh          #Para ejecutar el script
```
